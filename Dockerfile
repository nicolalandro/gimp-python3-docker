FROM python:3.8

RUN apt-get update && apt-get install -y flatpak
RUN flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

RUN flatpak install -y --user https://flathub.org/beta-repo/appstream/org.gimp.GIMP.flatpakref 

CMD flatpak run --command=bash org.gimp.GIMP//beta